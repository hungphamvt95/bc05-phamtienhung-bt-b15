function tinhDiemKhuVuc(khuVuc) {
  switch (khuVuc) {
    case "A":
      return 2;
      break;
    case "B":
      return 1;
      break;
      break;
    case "C":
      return 0.5;
      break;
    default:
      return 0;
      break;
  }
}

function tinhDiemDoiTuong(doiTuong) {
  switch (doiTuong) {
    case "1":
      return 2.5;
      break;
    case "2":
      return 1.5;
      break;
    case "3":
      return 0.5;
      break;
    default:
      return 0;
      break;
  }
}

function ketQua() {
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  console.log(' diemChuan: ',  diemChuan);
  var khuVuc = document.getElementById("txt-khu-vuc").value;
  var doiTuong = document.getElementById("txt-doi-tuong").value;
  var diemMon1 = document.getElementById("txt-diem-mon-1").value * 1;
  var diemMon2 = document.getElementById("txt-diem-mon-2").value * 1;
  var diemMon3 = document.getElementById("txt-diem-mon-3").value * 1;
  var diemKhuVuc = tinhDiemKhuVuc(khuVuc);
  var diemDoiTuong = tinhDiemDoiTuong(doiTuong);

  var diemTong3Mon = diemMon1 + diemMon2 + diemMon3 + diemKhuVuc + diemDoiTuong;

  if (diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0) {
    document.getElementById("result").innerHTML = `
   <p class="alert-danger"> Tổng điểm 3 môn: ${diemTong3Mon} </p> 
   <p class="alert-danger"> Kết quả: Thí sinh đã thi trượt <p> `;
  } else {
    if (diemTong3Mon >= diemChuan) {
      document.getElementById("result").innerHTML = ` 
      <p class="alert-success"> Tổng điểm 3 môn: ${diemTong3Mon} </p>
      <p class="alert-success"> Kết quả: Thí sinh đã thi đậu </p>  `;
    } else {
      document.getElementById("result").innerHTML = ` 
      <p class="alert-danger"> Tổng điểm 3 môn: ${diemTong3Mon} </p>
      <p class="alert-danger"> Kết quả: Thí sinh đã thi trượt </p>  `;
    }
  }
}
