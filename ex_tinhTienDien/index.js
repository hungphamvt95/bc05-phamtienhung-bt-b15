function tinh50KwDau(soKw) {
  var giaTien = soKw * 500;
  return giaTien;
}
function tinh50to100Kw(soKw) {
  var giaTien = 50 * 500 + (soKw - 50) * 650;
  return giaTien;
}
function tinh100to200Kw(soKw) {
  var giaTien = 50 * 500 + 50 * 650 + (soKw - 100) * 850;
  return giaTien;
}
function tinh200to350Kw(soKw) {
  var giaTien = 50 * 500 + 50 * 650 + 100 * 850 + (soKw - 200) * 1100;
  return giaTien;
}
function tinhTren350Kw(soKw) {
  var giaTien =
    50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (soKw - 350) * 1300;
  return giaTien;
}

function tinhTienDien() {
  var ten = document.getElementById("txt-ten").value;
  var soKwDaDung = document.getElementById("txt-so-kw").value * 1;
  var tienPhaiTra;
  if (soKwDaDung <= 50) {
    tienPhaiTra = tinh50KwDau(soKwDaDung);
  } else if (soKwDaDung <= 100) {
    tienPhaiTra = tinh50to100Kw(soKwDaDung);
  } else if (soKwDaDung <= 200) {
    tienPhaiTra = tinh100to200Kw(soKwDaDung);
  } else if (soKwDaDung <= 350) {
    tienPhaiTra = tinh200to350Kw(soKwDaDung);
  } else {
    tienPhaiTra = tinhTren350Kw(soKwDaDung);
  }
  console.log(new Intl.NumberFormat().format(tienPhaiTra));
  document.getElementById("result").innerHTML = ` 
  <p> Họ & Tên: ${ten} </p> 
  <p> Số tiền phải thanh toán: 
  ${new Intl.NumberFormat().format(tienPhaiTra)} VNĐ </p>`;
}
